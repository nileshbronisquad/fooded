//
//  TermsVC.swift
//  BookFast_Rider
//
//  Created by iMac on 28/01/21.
//

import UIKit
import WebKit

class TermsVC: UIViewController {

    @IBOutlet weak var wk: WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    var type = 1
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> TermsVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == 1 {
            //self.lblTitle.text = "Terms of Servi"
            self.wk.load(URLRequest(url: URL(string: "https://fooded.io/termsofservice")!))
        }
        else if type == 2 {
            //self.lblTitle.text = "Privacy Policy"
            self.wk.load(URLRequest(url: URL(string: "https://fooded.io/privacy")!))
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
   

}
