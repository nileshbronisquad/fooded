//
//  ChangeSubscriptionVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire

class ChangeSubscriptionVC: UIViewController {
    
    @IBOutlet weak var btnChoose2: UIButton!
    @IBOutlet weak var bgSub2: UIView!
    @IBOutlet weak var btnChoose: UIButton!
    @IBOutlet weak var bgSubscription: UIView!
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> ChangeSubscriptionVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ChangeSubscriptionVC") as! ChangeSubscriptionVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgSubscription.myViewCorners()
        self.bgSub2.myViewCorners()

        self.btnChoose.layer.cornerRadius = 6
        self.btnChoose.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        self.btnChoose.layer.shadowOpacity = 1
        self.btnChoose.layer.shadowOffset = .zero
        self.btnChoose.layer.shadowRadius = 5
        
        self.btnChoose2.layer.cornerRadius = 6
        self.btnChoose2.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        self.btnChoose2.layer.shadowOpacity = 1
        self.btnChoose2.layer.shadowOffset = .zero
        self.btnChoose2.layer.shadowRadius = 5
         
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
}
