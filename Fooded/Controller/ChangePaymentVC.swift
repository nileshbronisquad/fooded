//
//  ChangePaymentVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire

class ChangePaymentVC: UIViewController {
    
//    @IBOutlet weak var btnAdd: UIButton!
//    @IBOutlet weak var bg2: UIView!
//    @IBOutlet weak var bg1: UIView!
//    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnPayApple: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ChangePaymentVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ChangePaymentVC") as! ChangePaymentVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnPayApple.myViewCorners()
//        self.bg1.myViewCorners()
//        self.bg2.myViewCorners()
//
//        self.btnEdit.layer.cornerRadius = 6
//        self.btnEdit.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
//        self.btnEdit.layer.shadowOpacity = 1
//        self.btnEdit.layer.shadowOffset = .zero
//        self.btnEdit.layer.shadowRadius = 5
//
//        self.btnAdd.layer.cornerRadius = 6
//        self.btnAdd.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
//        self.btnAdd.layer.shadowOpacity = 1
//        self.btnAdd.layer.shadowOffset = .zero
//        self.btnAdd.layer.shadowRadius = 5
//
//        self.btnPayApple.layer.cornerRadius = 6
//        self.btnPayApple.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
//        self.btnPayApple.layer.shadowOpacity = 1
//        self.btnPayApple.layer.shadowOffset = .zero
//        self.btnPayApple.layer.shadowRadius = 5
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
