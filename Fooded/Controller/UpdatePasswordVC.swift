//
//  UpdatePasswordVC.swift
//  Fooded
//
//  Created by iMac on 18/02/21.
//

import UIKit
import Alamofire
import Toast_Swift
class UpdatePasswordVC: UIViewController {
    
    
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var btnPassword2: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnPassword1: UIButton!
    
    var isPass = false
    var isPass1 = false
    
    // MARK:- Create self View Controller object
    class func initVC() -> UpdatePasswordVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "UpdatePasswordVC") as! UpdatePasswordVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
              print(userId as Any)
        
        self.btnUpdate.applyShadowOnView()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdate(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.UpdatePasswordAPI()
            
        }else{
            self.showToastMessage(message: msg)
        }
        
    }
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtNewPassword.text == "" {
            msg = kEmptyPassword
        }
        else if txtConfirmPassword.text == ""{
            msg = kEmptyConfirmPassword
        }
        else if (txtNewPassword.text != txtConfirmPassword.text){
            msg = kPasswordAndConfirmNotSame
        }
        return msg
    }
    
    func UpdatePasswordAPI()  {
        let param = [
            "id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "new_password" : txtNewPassword.text!,
            "confirm_password" : txtConfirmPassword.text!] as [String:Any]
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_update_password",method: .post,parameters: param)
            
            .responseJSON { response in
                hideLoaderHUD()
                debugPrint(response)
                let response = response.value as! NSDictionary
                print(response)
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
        }
        
    }
    @IBAction func btnPass2(_ sender: Any) {
        
        if isPass1 {
            self.isPass1 = false
            self.btnPassword2.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtConfirmPassword.isSecureTextEntry = true
        }
        else {
            self.isPass1 = true
            self.btnPassword2.setImage(UIImage(named: "hide"), for: .normal)
            self.txtConfirmPassword.isSecureTextEntry = false
        }
    }
   
    @IBAction func btnPass1(_ sender: Any) {
        
        if isPass {
            self.isPass = false
            self.btnPassword1.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtNewPassword.isSecureTextEntry = true
        }
        else {
            self.isPass = true
            self.btnPassword1.setImage(UIImage(named: "hide"), for: .normal)
            self.txtNewPassword.isSecureTextEntry = false
        }
        
       
    }
    
}
