//
//  UpdateAddressVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire
class UpdateAddressVC: UIViewController {
    
    var arrUpdateAddress = [ModelUserData]()
    
    
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPostCode: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var bgAddress: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> UpdateAddressVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "UpdateAddressVC") as! UpdateAddressVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Update()
        btnEdit.applyShadowOnView()
        self.bgAddress.myViewCorners()
        self.btnEdit.layer.cornerRadius = 3
        self.btnEdit.layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        self.btnEdit.layer.shadowOpacity = 1
        self.btnEdit.layer.shadowOffset = .zero
        self.btnEdit.layer.shadowRadius = 5
        
        self.btnEdit.setTitle("Edit", for: .normal)
        
        self.txtAddress.isUserInteractionEnabled = false
        self.txtCity.isUserInteractionEnabled = false
        self.txtState.isUserInteractionEnabled = false
        self.txtPostCode.isUserInteractionEnabled = false
        
        
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        if btnEdit.titleLabel?.text == "Edit" {
            
            btnEdit.setTitle("Done", for: .normal)
            
            self.txtAddress.isUserInteractionEnabled = true
            self.txtPostCode.isUserInteractionEnabled = true
            self.txtState.isUserInteractionEnabled = true
            self.txtCity.isUserInteractionEnabled = true
            
        }
        else {
            
            btnEdit.setTitle("Edit", for: .normal)
            
            self.txtAddress.isUserInteractionEnabled = false
            self.txtCity.isUserInteractionEnabled = false
            self.txtState.isUserInteractionEnabled = false
            self.txtPostCode.isUserInteractionEnabled = false
            
            let msg = self.validation()
            if msg == "" {
                self.AddressUpdateAPI()
            }else{
                self.showToastMessage(message: msg)
            }
        }
        
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
         if
            txtAddress.text == ""{
            msg = kAddress
        }
        else if
            txtCity.text == ""{
            msg = kCity
        }
        else if
            txtState.text == ""{
            msg = kState
        }
        else if
            txtPostCode.text == ""{
            msg = kPostcode
        }
        return msg
    }
   
    func AddressUpdateAPI() {
        let param = ["id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
                   //  "name" :txtName.text!,
                     "address" : txtAddress.text!,
                     "city" : txtCity.text!,
                     "state" : txtState.text!,
                     "postcode" : txtPostCode.text!,
        ] as [String:Any]
        
        
        AF.request("http://vbsd.co.in/fooded/api/app_update_address" , method:.post,parameters: param)
            .responseJSON { response in
                
                
                let response = response.value as! NSDictionary
                
                if response.value(forKey: kStatus) as? Int ?? 0 == 1 {
                    let result = response.object(forKey: "result") as! [String:Any]
                    
                    print(result)
                    
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: result)
                    
                    model.saveToUserDefaults()
                    
                    self.Update()
                    
                    self.btnEdit.setTitle("Edit", for: .normal)
                    
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
            }
    }
    func Update()  {
        
        self.txtAddress.text = ModelUserData.getUserDataFromUserDefaults().address ?? ""
        self.txtPostCode.text =
            ModelUserData.getUserDataFromUserDefaults().postcode ?? ""
        self.txtState.text =
            ModelUserData.getUserDataFromUserDefaults().state ?? ""
        self.txtCity.text =
            ModelUserData.getUserDataFromUserDefaults().city ?? ""
    }
    
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
