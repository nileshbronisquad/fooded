//
//  CusineChoiceVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import Alamofire
import SDWebImage
import Toast_Swift

class CusineChoiceVC: UIViewController,UITableViewDelegate, UITableViewDataSource  {
   // var arrPrefrence = [PreferencesModel]()
// var arrUser = ["User 1","User 2","User 3"]
    
 var arrUser2 = [ModelPreferenceData]()
    @IBOutlet weak var tblView: UITableView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> CusineChoiceVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "CusineChoiceVC") as! CusineChoiceVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
     
        
       
        
        self.UserAPI()
        tblView.registerCell(withNib: "prefranceCell")
        self.tblView.reloadData()
        self.tblView.separatorStyle = .none
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrUser2.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : prefranceCell = tableView.dequeueReusableCell(withIdentifier: "prefranceCell") as! prefranceCell
        let data = self.arrUser2[indexPath.row]
        cell.lblUser.text = data.subUserName

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DietryRestrictionVC")as! DietryRestrictionVC
        
        vc.user = self.arrUser2[indexPath.row].subUserName
        vc.gotId = self.arrUser2[indexPath.row].id
        vc.mealCount = self.arrUser2[indexPath.row].mealPerDeliveryCount
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func UserAPI()  {
        
        let parm = [
            "user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
          
            ] as [String : Any]
        showLoaderHUD(strMessage: "")

        
        AF.request("http://vbsd.co.in/fooded/api/app_user_subscriptions_list" , method: .post ,parameters: parm)
            .responseJSON { response in
                hideLoaderHUD()
                self.showToastMessage(message: kdataLoded)
                debugPrint(response)
                
                let response = response.value as! NSDictionary
                print(response)
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    let result = response.value(forKey: "result")as! NSArray
                    for item in result {
                        
                        self.arrUser2.append(ModelPreferenceData.init(fromDictionary: item as! [String : Any]))
                        self.tblView.reloadData()
                    }
                    
                    
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }

        }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 100
//        
//    }
    

//
//    func preferences_list()  {
//        showLoaderHUD(strMessage: "")
//
//        AF.request("http://vbsd.co.in/fooded/api/app_preferences_list",method: .post).responseJSON { response in
//            hideLoaderHUD()
//            let response = response.value as! NSDictionary
//            print(response)
//            if response.value(forKey: "status") as? Int ?? 0 == 1 {
//
//                let result = response.value(forKey: "result")as! NSArray
//                for item in result {
//                   print(result)
//
//                    self.arrPrefrence.append(PreferencesModel.init(fromDictionary: item as! [String : Any]))
//
//                }
//                self.tblView.reloadData()
//            }
//            else {
//                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
//            }
//        }
//    }
}
}
