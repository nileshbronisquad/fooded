//
//  SignUpVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import Alamofire
import Toast_Swift
import MFSideMenu

@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SignUpVC: UIViewController {
    
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var txtPasswor: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    var isCheck = false
    var isPass = false
    
    // MARK:- Create self View Controller object
    class func initVC() -> SignUpVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnCountinue.applyShadowOnView()
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
        if userId == 0{
        }
        else{
            goToWelcome()
        }
        
    }
    @IBAction func btnTerms(_ sender: Any) {
        let vc = TermsVC.initVC()
        vc.type = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPrivacy(_ sender: Any) {
        let vc = TermsVC.initVC()
        vc.type = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnContinue(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.CallLoginAPI()
            
        }else{
            self.showToastMessage(message: msg)
        }
    }
    
    @IBAction func btnCheckBox(_ sender: Any) {
        
        if isCheck {
            isCheck = false
            self.btnCheckbox.setImage(UIImage(named: "check-box"), for: .normal)
        }
        else {
            isCheck = true
            self.btnCheckbox.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        
    }
    
    @IBAction func btnPassword(_ sender: Any) {
        
        if isPass {
            self.isPass = false
            self.btnPassword.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtPasswor.isSecureTextEntry = true
        }
        else {
            self.isPass = true
            self.btnPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtPasswor.isSecureTextEntry = false
        }
    }
    
    @IBAction func btnSignInAgain(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func CallLoginAPI()  {
        let param = [
            "mobile" : txtPhoneNumber.text!,
            "password" : txtPasswor.text!,
           // "token" :
        ]
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_register" , method: .post,parameters: param)
            .responseJSON { response in
                // debugPrint(response)
                hideLoaderHUD()
                let response = response.value as! NSDictionary
                if response.value(forKey: "status") as? Int ?? 0 == 1  {
                    
                    let result = response.object(forKey: "result") as! [String:Any]
                    
                    print(result)
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: result)
                    
                    model.saveToUserDefaults()
                    self.goToWelcome()
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                    
                }
                
        }
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtPhoneNumber.text == "" {
            msg = kEmptyUserName
        }
        else if txtPhoneNumber.text?.count != 10{
            msg = KValidPhone
        }
        else if txtPasswor.text == ""{
            msg = kEmptyPassword
        }
        else if isCheck {
            msg = kAcceptPolicy
        }
        
        return msg
    }
    
 func goToWelcome()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
       
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let rootNavigationController:UINavigationController = appdelegate.window?.rootViewController as! UINavigationController
            let navigationController:UINavigationController = storyBoard.instantiateViewController(withIdentifier: "navHome")  as! UINavigationController
            let sideMenuVc : SideMenuVC = storyBoard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            let menuContainer:MFSideMenuContainerViewController = MFSideMenuContainerViewController.container(withCenter: navigationController, leftMenuViewController: sideMenuVc, rightMenuViewController: nil)
            menuContainer.shadow.enabled = true
            menuContainer.panMode = MFSideMenuPanMode(rawValue: 1)
            rootNavigationController.pushViewController(menuContainer, animated: false)
    }
    
}
