//
//  SideMenuVC.swift
//  BookFast
//
//  Created by Nilesh Desai on 07/01/21.
//

import UIKit
import MFSideMenu

@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SideMenuVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    var arrTitle = ["Home", "Curation","Settings"]
    
    
    var aarImg = ["home","Shape 1","gear"]
    
    var aarImg2 = ["home-1","Shape-1","gear"]
    
    var aarImg3 = ["home-1","Shape 1","gear-1"]
    
    var isSelected = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.registerCell(withNib: "SideMenuCell")
        self.tblView.reloadData()
        self.tblView.separatorStyle = .none
        self.imgProfile.layer.masksToBounds = true
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        print(isSelected)
        
        self.tblView.reloadData()
        UpdateDetail()

    }
    
    
    @IBAction func btnHome(_ sender: Any)
    {
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        if isSelected == 2 {
            cell.img.image = UIImage(named: aarImg2[indexPath.row])
        }
        else if isSelected == 3 {
            cell.img.image = UIImage(named: aarImg3[indexPath.row])
        }
        else {
            
            cell.img.image = UIImage(named: aarImg[indexPath.row])
        }
        
        cell.lblTitle.text = arrTitle[indexPath.row]
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            self.isSelected = 1
            let vc = DashBoardVC.initVC()
            vc.isFrom = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1 {
            self.isSelected = 2
            let vc = CurationVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2 {
            self.isSelected = 3
            let vc = SettingVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func UpdateDetail() {
        
        let strImg = "http://vbsd.co.in/fooded/upload/profile/" + ModelUserData.getUserDataFromUserDefaults().userProfile
       self.imgProfile.setImageFromURL(strImg,UIImage(named: "profile placeholder")!)
               
              
        self.lblName.text =
            ModelUserData.getUserDataFromUserDefaults().name ?? ""
        self.lblEmail.text =
            ModelUserData.getUserDataFromUserDefaults().email ?? ""
        
    }
    
}

// MARK:-
extension UITableView {
    func registerCell(withNib reuseIdentifier:String) {
        self.register(UINib(nibName: reuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: reuseIdentifier)
    }
}
