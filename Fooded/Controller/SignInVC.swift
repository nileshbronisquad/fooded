//
//  SignInVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import MFSideMenu
import Alamofire
import Toast_Swift

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SignInVC: UIViewController {

//    var dic = [ModelUserData]()
    
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var bgFb: UIView!
    @IBOutlet weak var bgTwitter: UIView!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPhoneNumbwr: UITextField!
    var isPass = false
    
    // MARK:- Create self View Controller object
    class func initVC() -> SignInVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSignIn.applyShadowOnView()
        self.bgTwitter.myViewCorners()
        self.bgFb.myViewCorners()
        
        
    }

    @IBAction func btnPassword(_ sender: Any) {
        
        if isPass {
            self.isPass = false
            self.btnPassword.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }
        else {
            self.isPass = true
            self.btnPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtPassword.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func btnSignIn(_ sender: Any) {
       let msg = self.validation()
       if msg == "" {
        self.SignInAPI()
    
      //  UserDefaults.standard.set("1", forKey: "isLogin")

       }
       else{
           self.showToastMessage(message: msg)
       }
        
    }
    
    @IBAction func btnForgot(_ sender: Any) {
        let vc = ForgotPasswordVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        let vc = SignUpVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func SignInAPI()  {
        
        let param = ["mobile" : txtPhoneNumbwr.text!,
                     "password" : txtPassword.text!]
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_login" , method:.post,parameters: param)
            .responseJSON { response in
              
                hideLoaderHUD()
                let response = response.value as! NSDictionary
                

                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    
                    let result = response.object(forKey: "result") as! [String:Any]
                    
                    print(result)
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: result)
                    
                    model.saveToUserDefaults()
                    
                    self.goToWelcome()
            
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
               }
        }
    }
    
    func goToWelcome()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
       
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let rootNavigationController:UINavigationController = appdelegate.window?.rootViewController as! UINavigationController
            let navigationController:UINavigationController = storyBoard.instantiateViewController(withIdentifier: "navHome")  as! UINavigationController
            let sideMenuVc : SideMenuVC = storyBoard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            let menuContainer:MFSideMenuContainerViewController = MFSideMenuContainerViewController.container(withCenter: navigationController, leftMenuViewController: sideMenuVc, rightMenuViewController: nil)
            menuContainer.shadow.enabled = true
            menuContainer.panMode = MFSideMenuPanMode(rawValue: 1)
            rootNavigationController.pushViewController(menuContainer, animated: false)
    }
    func validation() -> String {
         self.view.endEditing(true)
         var msg = String()
         if txtPhoneNumbwr.text == "" { // Mobile
             msg = kEmptyMobile
         }
         else if txtPhoneNumbwr.text?.count != 10
         {
            msg = KValidPhone
         }
        else if
            txtPassword.text == ""{
            msg = kEmptyPassword
        }
         return msg
     }
     
}

extension UIView {
    //If you want only round corners
    func myViewCorners() {
        layer.cornerRadius = 8
        layer.masksToBounds = true
    }
    
    //If you want complete round shape, enable above comment line
    func myViewCorners(width:CGFloat) {
        layer.cornerRadius = width/2
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.red.cgColor
        layer.masksToBounds = true
    }
    
    func applyShadowOnView() {
        layer.cornerRadius = 8
        layer.shadowColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
    }
}
