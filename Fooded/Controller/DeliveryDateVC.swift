//
//  DeliveryDateVC.swift
//  Fooded
//
//  Created by macbook on 26/04/21.
//

import UIKit
import Alamofire
import SDWebImage
@available(iOS 13.0, *)
class DeliveryDateVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var txtDate: UITextField!
    class func initVC() -> DeliveryDateVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "DeliveryDateVC") as! DeliveryDateVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
  
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var userCollectionView: UICollectionView!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var datebgview: UIView!
    @IBOutlet weak var datebgVIew: UIView!
    
    var arrSelectedUser = ""
    var selectedIndex = Int ()
    var arrPreferences = [PreferencesModel]()
    var arrUser = [ModelPreferenceData]()
    var deliveryDate = ""
    var arrMonth = ""
    var arrSubscription = [SubscriptionModel]()
    var arrPrice = ""
    var avragePrice = 0
    var amount = 0
    var arrUserSelection = [[String]]()
    var arrOnlyUserId = [String]()
    var isReady = false
    var arrUserOrderData = [ModelOrderAdd]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ChangeSubscriptionValue()
        
        self.UserAPI()
        
        datebgVIew.myViewCorners()
        datebgview.myViewCorners()
        datebgview.layer.borderWidth = 1
        datebgview.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnNext.myViewCorners()
        btnCancel.myViewCorners()
        
        self.userCollectionView.register(UINib(nibName: "UserCollectionViewCell",bundle: nil), forCellWithReuseIdentifier: "UserCollectionViewCell")
        self.userCollectionView.reloadData()
        
        
        tbl.registerCell(withNib: "CusineChoiceCell")
        
        self.txtDate.text = deliveryDate
        
        self.tbl.reloadData()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrUser.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCollectionViewCell", for: indexPath as IndexPath)as! UserCollectionViewCell
        
        if selectedIndex == indexPath.row
        {
            Cell.bgViewUser.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
            Cell.lblUser.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else
        {
            Cell.bgViewUser.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            Cell.lblUser.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        
        
        let data = self.arrUser[indexPath.row]
        Cell.lblUser.text = data.subUserName
        
      
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100.0, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            selectedIndex = indexPath.row
        
            let data = arrUser[indexPath.row]
            self.arrSelectedUser = data.subUserName
            self.userCollectionView.reloadData()
 
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCollectionViewCell", for: indexPath as IndexPath)as! UserCollectionViewCell
            
            if selectedIndex == indexPath.row
            {
                myCell.bgViewUser.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
                myCell.lblUser.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               
                self.arrPreferences.removeAll()

                
            }
            else
            {
                myCell.bgViewUser.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                myCell.lblUser.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        
            self.preferencesAdd()
   
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPreferences.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CusineChoiceCell = tableView.dequeueReusableCell(withIdentifier: "CusineChoiceCell") as! CusineChoiceCell
        
        let name = arrPreferences [indexPath.row]
        cell.lblFoodname.text = name.preferencesName
        
        let strImg = "http://vbsd.co.in/fooded/upload/preferences/" + name.preferencesImage
        cell.imgFood.seetImageFromURL(strImg)
        let url = URL(string:strImg)
        let data = try? Data(contentsOf: url!)
        
        
        cell.btnPlus.tag = indexPath.row
        cell.btnPlus.addTarget(self, action: #selector(btnPlus), for: .touchUpInside)
        
        cell.btnMinus.tag = indexPath.row
        cell.btnMinus.addTarget(self, action: #selector(btnMinus), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    
    
        @objc func btnPlus(_ sender: UIButton)
        {
            let point = sender.convert(CGPoint.zero, to: tbl)
            if let indexPath = tbl.indexPathForRow(at: point) {
    
                let cell = tbl.cellForRow(at: indexPath) as? CusineChoiceCell
              
                var data = [String]()
                
                let str = String(self.arrUser[selectedIndex].id)
                let str1 = String(self.arrPreferences[indexPath.row].id)
                
                data.append(str)
                data.append(str1)
                data.append(cell!.lblValue.text!)
                
                for (i,item) in self.arrUserSelection.enumerated() {
                    if item[0] == str && item[1] == str1 {
                        self.arrUserSelection.remove(at: i)
                        break;
                    }
                }
                
                if cell!.lblValue.text! == "0" {
                    
                }
                else {
                    if self.arrOnlyUserId.contains(str) {
                        
                    }
                    else {
                        self.arrOnlyUserId.append(str)
                    }
                    self.arrUserSelection.append(data)
                }
                
                print(self.arrUserSelection)
              
            }
    
        }
    
    
    
        @objc func btnMinus(_ sender: UIButton)
        {
            let point = sender.convert(CGPoint.zero, to: tbl)
            if let indexPath = tbl.indexPathForRow(at: point) {
    
                let cell = tbl.cellForRow(at: indexPath) as? CusineChoiceCell
    
                var data = [String]()
                
                let str = String(self.arrUser[selectedIndex].id)
                let str1 = String(self.arrPreferences[indexPath.row].id)
                
                data.append(str)
                data.append(str1)
                data.append(cell!.lblValue.text!)
                
                for (i,item) in self.arrUserSelection.enumerated() {
                    if item[0] == str && item[1] == str1 {
                        self.arrUserSelection.remove(at: i)
                        break;
                    }
                }
                
                
                
                
                
                if cell!.lblValue.text! == "0" {
                    
                }
                else {
                    if self.arrOnlyUserId.contains(str) {
                        
                    }
                    else {
                        self.arrOnlyUserId.append(str)
                    }
                    
                    self.arrUserSelection.append(data)
                }
                
                
                
                print(self.arrUserSelection)
                
            }
    
        }
    
    
    func preferencesAdd()  {
        
        showLoaderHUD(strMessage: "")
        
        AF.request("http://vbsd.co.in/fooded/api/app_preferences_list",method: .post) .responseJSON { response in
            debugPrint(response)
            
            hideLoaderHUD()
            
            let response = response.value as! NSDictionary
            print(response)
        
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                let result = response.value(forKey: "result")as! NSArray
                for item in result {
                    self.arrPreferences.append(PreferencesModel.init(fromDictionary: item as! [String : Any]))
                    self.tbl.reloadData()
                    
                    
      
                }
            }
        }
        
        
    }
    
    func UserAPI()  {
        
        let parm = [
            "user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            
        ] as [String : Any]
        showLoaderHUD(strMessage: "")
        
        AF.request("http://vbsd.co.in/fooded/api/app_user_subscriptions_list" , method: .post ,parameters: parm)
            .responseJSON { response in
                hideLoaderHUD()
                
                
                let response = response.value as! NSDictionary
                print(response)
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    let result = response.value(forKey: "result")as! NSArray
                    for item in result {
                        
                        self.arrUser.append(ModelPreferenceData.init(fromDictionary: item as! [String : Any]))
                        self.userCollectionView.reloadData()
                    }
                    
                    self.preferencesAdd()
                    
                    
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
                
            }
    }
    
    func ChangeSubscriptionValue()  {
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_subscription_list",method: .post).responseJSON { response in
          hideLoaderHUD()
            let response = response.value as! NSDictionary
            print(response)
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                
                self.arrMonth =  ModelUserData.getUserDataFromUserDefaults()
                    .deliveryPerMonth ?? ""
                self.arrPrice =  ModelUserData.getUserDataFromUserDefaults().subscriptionPrice ?? ""
                
                print(self.arrMonth)
                print(self.arrPrice)
                
              

                let result = response.value(forKey: "result")as! NSArray
                for item in result {
                    
                    self.arrSubscription.append(SubscriptionModel.init(fromDictionary: item as! [String : Any]))
                    
                }
                
                
                for var i in (0..<self.arrSubscription.count)
                {
                    print(i)
                    
                   
                    if self.arrSubscription[i].subscriptionPrice == self.arrPrice {
                        
                        
                        self.avragePrice = self.arrSubscription[i].priceAverage ?? 0
                        
                        let meal = ModelUserData.getUserDataFromUserDefaults().mealsPerDelivery ?? ""
                       
                        
                        let Iav = Int(self.avragePrice)
                        let Ide = Int(meal)
                       
                        self.amount = (Iav * Ide!)
                         
                    }
  
                }
                
                
            }
            else {
                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
            }
        }
    }
    
    func UserOrdercheck()  {
        
        if isReady {
            self.UserOrderAdd()
        }
    }
    
    func UserOrderAdd()  {
        
        //1 - user id ---- id collection vali
        // 2 - preference id ----- id table vali
        //3 - quntity
        
        let parm = [
            "user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "restaurant_id": "6",
            "delivery_date":self.deliveryDate,
            "transaction_id":"78548758745",
            "amount":self.amount,
            "preferences_id":"\(self.arrUserSelection)"
            
        ] as [String : Any]
        
        
        showLoaderHUD(strMessage: "")
        
        AF.request("http://vbsd.co.in/fooded/api/app_order_add", method: .post ,parameters: parm)
            .responseData(completionHandler: { (response) in
                let str = String(data: response.data!, encoding: .utf8)
                print(str!)
                do {
                    let tmp = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String: Any]
                    print(tmp)
                } catch {
                    print(error)
                }
            })
            .responseJSON { response in
                hideLoaderHUD()
                
                debugPrint(response)
                
                let response = response.value as! NSDictionary
                print(response)
                
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    
                    self.arrUserOrderData.removeAll()
                    
                    let result = response.object(forKey: "order_data") as! [String:Any]
                    
                    self.arrUserOrderData.append(ModelOrderAdd.init(fromDictionary: result ))
                    
                    self.navigationController?.popViewController(animated: true)
                      
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
                
            }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func btnNext(_ sender: Any) {
        
        if  ModelUserData.getUserDataFromUserDefaults().address == "" {
            
            let vc = storyboard?.instantiateViewController(identifier: "UpdateAddressVC")as! UpdateAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else {
          
            for item1 in self.arrUser {
                
                let id = String(item1.id)
                
                if self.arrOnlyUserId.contains(id) {
                    self.isReady = true
                }
                else {
                    let name = item1.subUserName ?? ""
                    self.view.makeToast("Please select \(name) item")
                    self.isReady = false
                    break;
                }
                
            }
            
            self.UserOrdercheck()
                  
        }
      
         
    }
    
}

