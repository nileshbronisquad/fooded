//
//  ForgotPasswordVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import Alamofire
import Toast_Swift

@available(iOS 13.0, *)
class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnSumbit: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ForgotPasswordVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPhoneNumber.text = ModelUserData.getUserDataFromUserDefaults().mobile ?? ""
        
        self.btnSumbit.applyShadowOnView()
    }
    
    
    @IBAction func btnSubmit(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.ForgetPasswordAPI()
            
        }else{
            self.showToastMessage(message: msg)
    
        }
    }
    
    
    
    func validation() -> String {
        
        self.view.endEditing(true)
        var msg = String()
        if txtPhoneNumber.text == "" {
            msg = kEmptyMobile
        
        }
        
        else if txtPhoneNumber.text?.count != 10{
            msg = KValidPhone
        
        }
        
        return msg
    
    }
    
    
    func ForgetPasswordAPI() {
        
        let param = [
            "mobile" : txtPhoneNumber.text!,
        ]
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_forgot_password" , method: .post,parameters: param)
            .responseJSON { response in
                hideLoaderHUD()
                debugPrint(response)
                let response = response.value as! NSDictionary
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    
                    let result = response.object(forKey: "result") as! [String:Any]
                    
                    print(result)
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: result)
                    
                    model.saveToUserDefaults()
                    let vc = OTPVC.initVC()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                
                }
        }
    }

    @IBAction func btnSignIn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
