//
//  UpdateSettingVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire
import Toast_Swift

class UpdateSettingVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var txtMain: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    
    var isFromPassword = false
    
    var Title = ""
    var emailTitle = ""
    var placeholder = ""
    
   
    // MARK:- Create self View Controller object
    class func initVC() -> UpdateSettingVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "UpdateSettingVC") as! UpdateSettingVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnSubmit.applyShadowOnView()
        
        if isFromPassword {
            
            self.lblTitle.text =  Title
            self.lblEmailTitle.text = emailTitle
            self.txtMain.placeholder = placeholder
            self.btnEye.isHidden = false
            self.txtMain.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
 
    }
    
   
    @IBAction func btnSubmit(_ sender: Any) {
        
    }
    
}
