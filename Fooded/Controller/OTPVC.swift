//
//  OTPVC.swift
//  Fooded
//
//  Created by macbook on 19/04/21.
//

import UIKit
import OTPFieldView
@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class OTPVC: UIViewController {
    class func initVC() -> OTPVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    var strOTP = String()
    var count = 60  // 60sec if you want
    var resendTimer = Timer()
    var strMobile = ""
    
    @IBOutlet weak var lblDownTimer: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var viewOTP: OTPFieldView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnVerify.applyShadowOnView()
        self.lblMobile.text = strMobile
        self.setupOtpView()
        self.btnVerify.isEnabled = false
        self.btnResend.setTitleColor(#colorLiteral(red: 0.02352941176, green: 0.4941176471, blue: 0.6745098039, alpha: 0.5), for: .normal)
        self.btnResend.isEnabled = false
        self.startTimer()
        self.lblMobile.text = ModelUserData.getUserDataFromUserDefaults().mobile ?? ""
        self.btnVerify.applyShadowOnView()

        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.resendTimer.invalidate()
    }
    func startTimer() {
        
        resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    @objc func update() {
        if(count > 0) {
            count = count - 1
            print(count)
            self.btnResend.isEnabled = false
            self.btnResend.setTitleColor(#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), for: .normal)
            self.lblDownTimer.isHidden = false
            self.lblDownTimer.text = "\(count)"
        }
        else {
            resendTimer.invalidate()
            print("call your api")
            self.btnResend.isEnabled = true
            self.btnResend.setTitleColor(#colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1), for: .normal)
            self.lblDownTimer.isHidden = true
        }
    }
    
    func setupOtpView(){
        
        self.viewOTP.fieldsCount = 4
        self.viewOTP.fieldBorderWidth = 2
        self.viewOTP.defaultBorderColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        self.viewOTP.filledBorderColor = #colorLiteral(red: 0.1568627451, green: 0.1843137255, blue: 0.2235294118, alpha: 1)
        self.viewOTP.cursorColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
        self.viewOTP.displayType = .underlinedBottom
        self.viewOTP.fieldSize = 40
        self.viewOTP.separatorSpace = 20
        self.viewOTP.shouldAllowIntermediateEditing = false
        self.viewOTP.delegate = self
        self.viewOTP.initializeUI()
        
    }
    @IBAction func btnVerify(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "ResetpasswordVC")as! ResetpasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnResend(_ sender: Any) {
        

    }
}
@available(iOS 13.0, *)
extension OTPVC : OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        self.strOTP = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        if hasEnteredAll {
            //API CALLING
            self.btnVerify.isEnabled = true
         //   self.bgViewSubmit.backgroundColor = #colorLiteral(red: 0, green: 0.6799505949, blue: 0.8742008805, alpha: 1)
           // self.callVerificationAPI()
        }
        else {
            
            self.btnVerify.isEnabled = false
          //  self.bgViewSubmit.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.8705882353, blue: 0.8901960784, alpha: 1)
            return false
        }
        return false
        
    }
}
