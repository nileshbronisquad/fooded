//
//  ResetpasswordVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire
import Toast_Swift

@available(iOS 13.0, *)
class ResetpasswordVC: UIViewController {
    
    @IBOutlet weak var txtConPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnPassword2: UIButton!
    @IBOutlet weak var btnPassword1: UIButton!
    @IBOutlet weak var btnResetPass: UIButton!
    
    var isPass = false
    var isPass1 = false
    
  
    // MARK:- Create self View Controller object
    class func initVC() -> ResetpasswordVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ResetpasswordVC") as! ResetpasswordVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnResetPass.applyShadowOnView()
        
     
    }
    
    
    @IBAction func btnReset(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.ResetPasswordAPI()
            
        }else{
            self.showToastMessage(message: msg)
        }
        
    }
    
  func validation() -> String {
         self.view.endEditing(true)
         var msg = String()
         if txtPassword.text == "" {

             msg = kEmptyPassword
         }
         else if txtConPassword.text == ""{
            msg = kEmptyConfirmPassword
    }
         else if (txtPassword.text != txtConPassword.text){
            msg = kPasswordAndConfirmNotSame
    }
        
         return msg
     }
    
    func ResetPasswordAPI()  {
        
        let parm = [
            "id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "new_password" : txtPassword.text!,
            "confirm_password" : txtConPassword.text!
            
            ] as [String : Any]
        showLoaderHUD(strMessage: "")

        
        AF.request("http://vbsd.co.in/fooded/api/app_update_password" , method: .post ,parameters: parm)
            .responseJSON { response in
                hideLoaderHUD()
                debugPrint(response)
                
                let response = response.value as! NSDictionary
                print(response)
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                   
                    let vc = self.storyboard?.instantiateViewController(identifier: "SignInVC")as! SignInVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
                
        }
    }
    func updateData()  {
       
       
       let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
       
       print(userId)
    }
    @IBAction func btnPassword1(_ sender: Any) {
        
        if isPass {
            self.isPass = true
            self.btnPassword1.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }
        else {
            self.isPass = false
            self.btnPassword1.setImage(UIImage(named: "hide"), for: .normal)
            self.txtPassword.isSecureTextEntry = false
        }
        
    }
    
    
    @IBAction func btnPassword2(_ sender: Any) {
        
        if isPass1 {
            self.isPass1 = true
            self.btnPassword2.setImage(UIImage(named: "Forma 1"), for: .normal)
            self.txtConPassword.isSecureTextEntry = true
        }
        else {
            self.isPass1 = false
            self.btnPassword2.setImage(UIImage(named: "hide"), for: .normal)
            self.txtConPassword.isSecureTextEntry = false
        }
        
    }
    
}
