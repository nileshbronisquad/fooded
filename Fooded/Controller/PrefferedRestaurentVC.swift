//
//  PrefferedRestaurentVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import Alamofire
import SDWebImage
class PrefferedRestaurentVC: UIViewController,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
    
    var arrRestaurant = [Restaurant]()
    var arrRestaurantSearch = [Restaurant]()
    var gotId = 0
    var status = ""
    @IBOutlet weak var txtSearchbar: UITextField!
    @IBOutlet weak var bgSearch: UIView!
    @IBOutlet weak var tblView: UITableView!
    var rowsWhichAreChecked = [NSIndexPath]()
    
    // MARK:- Create self View Controller object
    class func initVC() -> PrefferedRestaurentVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "PrefferedRestaurentVC") as! PrefferedRestaurentVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
        UserList()
        self.bgSearch.myViewCorners()
        tblView.registerCell(withNib: "PrefferedRestCell")
        self.tblView.reloadData()
        self.tblView.separatorStyle = .none
        
        txtSearchbar.addTarget(self, action: #selector(searchedRecord), for: .editingChanged)
        txtSearchbar.delegate = self
       
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtSearchbar {
            self.view.endEditing(true)
            if txtSearchbar.text != "" {
                self.ReturnPressCall()
            }
        }
        return true
    }
    
    @objc func searchedRecord (sender: UITextField)
    {
        self.ReturnPressCall()
    }
    
    func ReturnPressCall() {
        
        if txtSearchbar.text != "" {
            self.arrRestaurantSearch = self.arrRestaurant.filter { $0.restaurantName.lowercased().contains(self.txtSearchbar.text!) }
            
            let range  = txtSearchbar.text?.lowercased().range(of: txtSearchbar.text!, options: .caseInsensitive, range: nil, locale: nil)
            
            if range != nil {
                self.arrRestaurantSearch.append(contentsOf: self.arrRestaurant.filter { $0.restaurantName.contains(self.txtSearchbar.text!) })
            }
           
            print(self.arrRestaurantSearch.count)
        }
        else {
            self.view.endEditing(true)
            UserList()
        }
        self.tblView.reloadData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrRestaurantSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PrefferedRestCell = tableView.dequeueReusableCell(withIdentifier: "PrefferedRestCell") as! PrefferedRestCell
       
        
        let path = self.arrRestaurantSearch[indexPath.row]
        
        if path.isSelect == "1" {
            cell.btnCheckBox.setImage(UIImage(named: "check-box"), for: .normal)
        }
        else {
            cell.btnCheckBox.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        
        cell.lblRestaurantName.text = path.restaurantName
        cell.lblRestaurantType.text = path.restaurantType
        cell.lblRestaurantAddress.text = path.restaurantAddress
        cell.lblRating.text = path.restaurantRatingCount
        cell.ratingView.rating = Float(path.restaurantRatingCount!)!
        
       
        
        let strImg = "http://vbsd.co.in/fooded/upload/restaurant/" + path.restaurantImage
        cell.imgRestaurant.seetImageFromURL(strImg)
        let url = URL(string:strImg)
        let data = try? Data(contentsOf: url!)
        
        
            
            if self.arrRestaurant[0].isSelect == "0" {
                cell.isbtn = true
                
            }
            else {
                cell.isbtn = false
            }
            
            cell.btnCheckbox((UIButton).self)
            
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:PrefferedRestCell = tableView.cellForRow(at: indexPath) as! PrefferedRestCell
        
        if(rowsWhichAreChecked.contains(indexPath as NSIndexPath) == false){
            cell.isbtn = true
            
            
            cell.btnCheckBox.setImage(UIImage(named: "check-box"), for: .normal)
            rowsWhichAreChecked.append(indexPath as NSIndexPath)
            
            let data = self.arrRestaurant[indexPath.row]
            self.gotId = data.id
            if data.isSelect == "1" {
                status = "0"
            }
            else {
                status = "1"
            }
            
            
            self.RestaurantAdd()
        }else{
            cell.isbtn = false
            status = "0"
            cell.btnCheckBox.setImage(UIImage(named: "Uncheck"), for: .normal)
            
            let data = self.arrRestaurant[indexPath.row]
            self.gotId = data.id
            
            if data.isSelect == "1" {
                status = "0"
            }
            else {
                status = "1"
            }
            
            self.RestaurantAdd()
            if let checkedItemIndex = rowsWhichAreChecked.firstIndex(of: indexPath as NSIndexPath){
                rowsWhichAreChecked.remove(at:checkedItemIndex)
            }
        }
    }
    
    func UserList()  {
        let param = ["rating_user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0 ]
        showLoaderHUD(strMessage: "")
        
        AF.request("http://vbsd.co.in/fooded/api/app_restaurant_list",method: .post,parameters: param) .responseJSON { response in
            hideLoaderHUD()
            debugPrint(response)
            let response = response.value as! NSDictionary
            print(response)
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                
                let result = response.value(forKey: "result")as! [[String : Any]]
                
                self.arrRestaurant.removeAll()
                self.arrRestaurantSearch.removeAll()
                self.arrRestaurant = result.map(Restaurant.init)
                self.arrRestaurantSearch = self.arrRestaurant
                self.tblView.reloadData()
            }
            else
            {
                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
            }
        }
        
    }
    
    func RestaurantAdd()  {
        
        let param = [
            "user_id":ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "resturant_id": gotId,
            "resturant_status": status
        ] as [String : Any]
        
        AF.request("http://vbsd.co.in/fooded/api/app_user_restaurant_status",method: .post,parameters: param) .responseJSON { response in
            debugPrint(response)
            
            self.UserList()
        }
    }
}
extension UIImageView {

     //Set Web Image
    func seetImageFromURL(_ urlString: String) {
        self.sd_setImage(with: URL(string: urlString))
    }

}
