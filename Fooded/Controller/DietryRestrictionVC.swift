//
//  DietryRestrictionVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import Alamofire

class DietryRestrictionVC: UIViewController {

    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblVegan: UILabel!
    @IBOutlet weak var lblGlutenFree: UILabel!
    @IBOutlet weak var lblKetogenic: UILabel!
    @IBOutlet weak var lblLactoseintolerant: UILabel!
    @IBOutlet weak var lblpaleogenic: UILabel!
    @IBOutlet weak var lblAllergenfriendly: UILabel!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    
    var user = ""
    var gotId = 0
    var mealCount = ""
    var arrUser2 = [ModelPreferenceData]()

    var isbtn1 = false
    var isbtn2 = false
    var isbtn3 = false
    var isbtn4 = false
    var isbtn5 = false
    var isbtn6 = false
    
    var vegan = ""
    var GlutenFree = ""
    var Ketogenic = ""
    var Lactoseintolerant = ""
    var paleogenic = ""
    var Allergenfriendly = ""
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> CusineChoiceVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "CusineChoiceVC") as! CusineChoiceVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UserList()
        lblUser.text = user
    }
    
    func setUpUI() {
        
        if self.arrUser2[0].isVegan == "0" {
            self.isbtn1 = true
            
        }
        else {
            self.isbtn1 = false
        }
        
        if self.arrUser2[0].isGivtenFree == "0" {
            self.isbtn2 = true
            
        }
        else {
            self.isbtn2 = false
        }
        
        if self.arrUser2[0].isKetogenic == "0" {
            self.isbtn3 = true
            
        }
        else {
            self.isbtn3 = false
        }
        
        if self.arrUser2[0].isLactoseIntrollerant == "0" {
            self.isbtn4 = true
            
        }
        else {
            self.isbtn4 = false
        }
        
        if self.arrUser2[0].isPaleogenic == "0" {
            self.isbtn5 = true
            
        }
        else {
            self.isbtn5 = false
        }
        
        if self.arrUser2[0].isAllergenFriendly == "0" {
            self.isbtn6 = true
            
        }
        else {
            self.isbtn6 = false
        }
        
        self.btn1((UIButton).self)
        self.btn2((UIButton).self)
        self.btn3((UIButton).self)
        self.btn4((UIButton).self)
        self.btn5((UIButton).self)
        self.btn6((UIButton).self)
        
    }

    
    @IBAction func btn1(_ sender: Any) {
        if isbtn1 {
            isbtn1 = false
            vegan = "0"
            self.btn1.setImage(UIImage(named: "Uncheck"), for: .normal)
        
        }
        else {
            vegan = "1"
            isbtn1 = true
            self.btn1.setImage(UIImage(named: "check-box"), for: .normal)
        }
    }
    
    @IBAction func btn2(_ sender: Any) {
        if isbtn2 {
            isbtn2 = false
            GlutenFree = "0"
            self.btn2.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        else {
            GlutenFree = "1"

            isbtn2 = true
            self.btn2.setImage(UIImage(named: "check-box"), for: .normal)
        }
    }
    
    @IBAction func btn3(_ sender: Any) {
        if isbtn3 {
            isbtn3 = false
            Ketogenic = "0"
            self.btn3.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        else {
            Ketogenic = "1"

            isbtn3 = true
            self.btn3.setImage(UIImage(named: "check-box"), for: .normal)
        }
    }
    
    @IBAction func btn4(_ sender: Any) {
        
        if isbtn4 {
            isbtn4 = false
            Lactoseintolerant = "0"
            self.btn4.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        else {
            Lactoseintolerant = "1"

            isbtn4 = true
            self.btn4.setImage(UIImage(named: "check-box"), for: .normal)
        }
    }
    @IBAction func btn5(_ sender: Any) {

        if isbtn5 {
            isbtn5 = false
            paleogenic = "0"
            self.btn5.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
            else{
                paleogenic = "1"
                isbtn5 = true
                self.btn5.setImage(UIImage(named: "check-box"), for: .normal)
            }
        }
    
    @IBAction func btn6(_ sender: Any) {
        if isbtn6 {
            isbtn6 = false
            Allergenfriendly = "0"
            self.btn6.setImage(UIImage(named: "Uncheck"), for: .normal)
        }
        else {
            Allergenfriendly = "1"

            isbtn6 = true

            self.btn6.setImage(UIImage(named: "check-box"), for: .normal)
        }
    }
   
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func btnUpdate(_ sender: Any) {
        self.UpdateUserList()
    }
     
    func UserList()  {
        let param = ["id" : gotId]
        AF.request("http://vbsd.co.in/fooded/api/app_user_subscriptions_single_list",method: .post,parameters: param) .responseJSON { response in
        
            let response = response.value as! NSDictionary
            print(response)
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                let result = response.value(forKey: "result") as! NSDictionary
                    self.arrUser2.append(ModelPreferenceData.init(fromDictionary: result as! [String : Any]))
                
                self.setUpUI()
                
            }
            else {
                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
            }
        
        }
        
    }
    
   
    func UpdateUserList()  {
        let param = [
            "id": gotId,
            "user_id":ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "sub_user_name": user,
            "meal_per_delivery_count":mealCount,
            "is_vegan" : vegan,//lblVegan.text!,
            "is_givten_free":GlutenFree,//lblGlutenFree.text!,
            "is_ketogenic":Ketogenic,//lblKetogenic.text!,
            "is_lactose_introllerant":Lactoseintolerant, //lblLactoseintolerant.text!,
            "is_paleogenic":paleogenic,//lblpaleogenic.text!,
            "is_allergen_friendly":Allergenfriendly//lblAllergenfriendly.text!
        ] as [String : Any]
        
        AF.request("http://vbsd.co.in/fooded/api/app_user_subscriptions_update",method: .post,parameters: param) .responseJSON { response in
        
            debugPrint(response)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}
