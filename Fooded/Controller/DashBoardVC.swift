//
//  DashBoardVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit
import FSCalendar
import Alamofire
import SDWebImage

@available(iOS 13.0, *)
@available(iOS 13.0, *)
@available(iOS 13.0, *)
class DashBoardVC: UIViewController, FSCalendarDelegate,FSCalendarDataSource {
    
    var isFrom = true
    
    // MARK:- Create self View Controller object
    class func initVC() -> DashBoardVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    @IBOutlet weak var FsCalander: FSCalendar!
    var arrOrdrerList = [ModelOrderList]()
    var datesWithMultipleEvents = ["2021-04-30", "2015-10-16", "2015-10-20", "2015-10-28"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FsCalander.delegate = self
        FsCalander.dataSource = self
        
        if  ModelUserData.getUserDataFromUserDefaults().subscriptionStatus == "1" {
        }
        else{
            Alert()
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.OrderList()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        

        for dateStr in self.arrOrdrerList{
            
            let final = dateStr.deliveryDate
            let dateSplit = final?.split(separator: "-")
            let concet = dateSplit![2] + "/" + dateSplit![1] + "/" + dateSplit![0]
            
            print(concet)
            
            if(dateFormatter.string(from: date) == concet)
            {
                return 1
            }
        }
        return 0
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if  ModelUserData.getUserDataFromUserDefaults().subscriptionStatus == "1" {
            
            let deliveryMonth = ModelUserData.getUserDataFromUserDefaults()
                .deliveryPerMonth ?? ""
            
            var strDates = [String]()
            
            for item in self.arrOrdrerList {
                strDates.append(item.deliveryDate)
            }
            
            let strSelectionDate = self.formatter.string(from: date)
            
            let strSelectionDate1 = self.formatter1.string(from: date)
            
            if deliveryMonth == "\(strDates.count)" {
                self.view.endEditing(true)
                self.view.makeToast("You have already choose current month delivery")
            }
            else {
                
                if strDates.contains(strSelectionDate1) {
                    self.view.makeToast("You have already choose current date delivery")
                }
                else {
                   
                    let vc = storyboard?.instantiateViewController(identifier: "DeliveryDateVC")as! DeliveryDateVC
                    vc.deliveryDate = strSelectionDate
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
            
        }
        else{
            Alert()
            
        }

        }
    
    
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let result = formatter.string(from: date)
        print(result)
        return self.formatter.date(from: result)!
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let calendar1 = NSCalendar.current
        let comps2 = NSDateComponents()
        comps2.month = 1
        comps2.day = -1
        let startMonth = Date().startOfMonth()
        let endOfMonth = calendar1.date(byAdding: comps2 as DateComponents, to: startMonth)
        let result = self.formatter.string(from: endOfMonth!)
        return self.formatter.date(from: result)!
    }
    
    
    @IBAction func btnSideMenu(_ sender: Any) {
        
        if isFrom {
            menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
        
        
    }
    
    func Alert()  {
        
        let alert = UIAlertController(title: "Subcription", message: "Please select the subcription for meal", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler:{ (alert: UIAlertAction!) in
           
            let vc = ChangeSubscriptionNewVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
            

        })

        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        yes.setValue(UIColor.init(cgColor: #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)), forKey: "titleTextColor")
        no.setValue(UIColor.init(cgColor: #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)), forKey: "titleTextColor")
        
        
        alert.addAction(yes)
        alert.addAction(no)
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func OrderList()  {
        
        let param = ["user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0 ]
        showLoaderHUD(strMessage: "")
        
        AF.request("http://vbsd.co.in/fooded/api/app_order_list",method: .post,parameters: param) .responseJSON { response in
            hideLoaderHUD()
            
            let response = response.value as! NSDictionary
            print(response)
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                
                let result = response.value(forKey: "result")as! [[String : Any]]
                
                self.arrOrdrerList.removeAll()
                self.arrOrdrerList = result.map(ModelOrderList.init)
                
                self.FsCalander.delegate = self
                self.FsCalander.dataSource = self
                
                
                
            }
            else
            {
                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
            }
        }
        
    }
}

extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

