//
//  SettingVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit

class SettingVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tblView: UITableView!
    var arrTitle = ["Update Profile","Update Password","Update Payment","Update Address","Change subscription","Logout"]
    
    // MARK:- Create self View Controller object
    class func initVC() -> SettingVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.registerCell(withNib: "SideMenuCell")
        self.tblView.reloadData()
        self.tblView.separatorStyle = .none
        
        print("Setting")
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
     
        cell.lblTitle.font = UIFont(name: "Quicksand-Regular", size:18)
        cell.lblTitle.text = arrTitle[indexPath.row]
        
        cell.img.isHidden = true
        
        cell.lblLead.constant = -4
        cell.imgWidth.constant = 0
        cell.arrow.isHidden = false
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let vc = UpdateProfileVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1 {
            let vc = UpdatePasswordVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2 {
            let vc = ChangePaymentVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3 {
            let vc = UpdateAddressVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 4 {
            let vc = ChangeSubscriptionNewVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 5 {
            USERDEFAULTS.removeObject(forKey: kModelUserData)
            USERDEFAULTS.synchronize()
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
   

}
