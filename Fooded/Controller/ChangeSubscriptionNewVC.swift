//
//  ChangeSubscriptionNewVC.swift
//  Fooded
//
//  Created by iMac on 18/02/21.
//

import UIKit
import Alamofire
import Toast_Swift

class ChangeSubscriptionNewVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    var arrSubscription = [SubscriptionModel]()
    var arrmodel = [ModelUserData]()
    var arrMonth = ""
    var arrPrice = ""
    var selectedIndex = Int ()
    var selectedIndex1 = Int ()
    var arrtotal = ""
    var avragePrice = 0
    var deliveryPerMonth = ""
    
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var PrizeCollectionView: UICollectionView!
    @IBOutlet weak var MonthCollectionView: UICollectionView!
  
    @IBOutlet weak var lbltotalPrice: UILabel!
    @IBOutlet weak var txtUpdate: UITextField!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var bgPerMonth: UIView!
    @IBOutlet weak var bgStepper: UIView!
    @IBOutlet weak var bgPrice: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ChangeSubscriptionNewVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ChangeSubscriptionNewVC") as! ChangeSubscriptionNewVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.MonthCollectionView.reloadData()
        self.PrizeCollectionView.reloadData()
        
        self.updateData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ChangeSubscriptionValue()
        
        self.MonthCollectionView.register(UINib(nibName: "MonthCollectionViewCell",bundle: nil), forCellWithReuseIdentifier: "MonthCollectionViewCell")
        
        self.PrizeCollectionView.register(UINib(nibName: "PriceCollectionViewCell",bundle: nil), forCellWithReuseIdentifier: "PriceCollectionViewCell")
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
       
        self.arrMonth =  ModelUserData.getUserDataFromUserDefaults().deliveryPerMonth
        self.arrPrice =  ModelUserData.getUserDataFromUserDefaults().subscriptionPrice
        if ModelUserData.getUserDataFromUserDefaults().mealsPerDelivery == "" {
            self.lblValue.text = "0"
        }
        else {
            self.lblValue.text = ModelUserData.getUserDataFromUserDefaults().mealsPerDelivery
        }
        

        
        self.bgPrice.myViewCorners()
        self.bgStepper.myViewCorners()
        self.bgPerMonth.myViewCorners()
        
        
    }
    
    
    @IBAction func txtUpdate(_ sender: Any) {
        
        let month = Int(arrMonth)
        let prize = Int(arrPrice)
        let value = Int(lblValue.text!)
        let total = month! * prize! * value!
        txtUpdate.text = "$\(total)"
        arrtotal = txtUpdate.text!

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.MonthCollectionView
        {
            return self.arrSubscription.count
        }
        else{
            return self.arrSubscription.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.MonthCollectionView {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthCollectionViewCell", for: indexPath as IndexPath)as! MonthCollectionViewCell
            
            if selectedIndex == indexPath.row
            {
                myCell.bgviewMonth.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
                myCell.lblMonth.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                myCell.bgviewMonth.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                myCell.lblMonth.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            let data = self.arrSubscription[indexPath.row]
            myCell.lblMonth.text = data.deliveryPerMonth
            return myCell
        } else {
            let myCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "PriceCollectionViewCell", for: indexPath as IndexPath)as!PriceCollectionViewCell
            
            
            if selectedIndex1 == indexPath.row
            {
                myCell2.bgViewPrize.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
                myCell2.lblPrize.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                myCell2.bgViewPrize.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                myCell2.lblPrize.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            let data = self.arrSubscription[indexPath.row]
            
            myCell2.lblPrize.text = data.subscriptionPrice
            
            return myCell2
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.MonthCollectionView {
            return 60
        }
        else{
            return 60
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.MonthCollectionView {
            return 1
        }
        else{
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.MonthCollectionView
        {
            selectedIndex = indexPath.row
        
            let data = arrSubscription[indexPath.row]
            self.arrMonth = data.deliveryPerMonth
            self.MonthCollectionView.reloadData()
 
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthCollectionViewCell", for: indexPath as IndexPath)as! MonthCollectionViewCell
            
            if selectedIndex == indexPath.row
            {
                myCell.bgviewMonth.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
                myCell.lblMonth.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                myCell.bgviewMonth.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                myCell.lblMonth.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
   
        }
        else           {
            selectedIndex1 = indexPath.row
            let data = arrSubscription[indexPath.row]
            self.arrPrice = data.subscriptionPrice
            self.PrizeCollectionView.reloadData()
            
            let myCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "PriceCollectionViewCell", for: indexPath as IndexPath)as!PriceCollectionViewCell
            
            
            if selectedIndex1 == indexPath.row
            {
                myCell2.bgViewPrize.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
                myCell2.lblPrize.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                myCell2.bgViewPrize.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                myCell2.lblPrize.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
        
        
    }
    func ChangeSubscriptionValue()  {
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_subscription_list",method: .post).responseJSON { response in
          hideLoaderHUD()
            let response = response.value as! NSDictionary
            print(response)
            if response.value(forKey: "status") as? Int ?? 0 == 1 {
                
                self.arrSubscription.removeAll()
                
                self.arrMonth =  ModelUserData.getUserDataFromUserDefaults()
                    .deliveryPerMonth ?? ""
                
                self.arrPrice =  ModelUserData.getUserDataFromUserDefaults().subscriptionPrice ?? ""

                let result = response.value(forKey: "result")as! NSArray
                for item in result {
                    
                    self.arrSubscription.append(SubscriptionModel.init(fromDictionary: item as! [String : Any]))
                    
                }
                
                
                for var i in (0..<self.arrSubscription.count)
                {
                    print(i)
                    
                    if self.arrSubscription[i].deliveryPerMonth == self.arrMonth {
                        self.selectedIndex = i
                    }
                    
                    if self.arrSubscription[i].subscriptionPrice == self.arrPrice {
                        self.selectedIndex1 = i
                        
                        self.avragePrice = self.arrSubscription[i].priceAverage ?? 0
                        self.deliveryPerMonth = self.arrSubscription[i].deliveryPerMonth ?? ""
                        let value = self.lblValue.text!
                        
                        let Iav = Int(self.avragePrice)
                        let Ide = Int(self.deliveryPerMonth)
                   
                        let cal = (Iav * Ide!) * (Int(value)!)
                        
                        self.lbltotalPrice.text = "$ \(cal)"
                        
                    }
  
                }
                
                
                self.MonthCollectionView.reloadData()
                self.PrizeCollectionView.reloadData()
                
                
            }
            else {
                self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
            }
        }
    }
    
    func AddSubscription()  {
        let parm = ["user_id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
        "delivery_per_month" : arrMonth,
        "subscription_price":arrPrice,
        "meals_per_delivery":lblValue.text!
        ] as [String : Any] 
        showLoaderHUD(strMessage: "")

        AF.request("http://vbsd.co.in/fooded/api/app_user_subscriptions_add",method: .post,parameters: parm).responseJSON
                { response in
                hideLoaderHUD()
                debugPrint(response)
            let response = response.value as! NSDictionary
                print(response)  
                if response.value(forKey: "status") as? Int ?? 0 == 1 {
                    
                    let result = response.value(forKey: "result")
                    let model : ModelUserData = ModelUserData(fromDictionary: result as! [String : Any])
         
                    model.saveToUserDefaults()
                    self.updateData()
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.showToastMessage(message: response.value(forKey: kMessage) as? String ?? "")
                }
            }
    }
    func validation() -> String {
            self.view.endEditing(true)
            var msg = String()
            if lblValue.text == "" {
                msg = kEnterMealsPerDelivery
            }
        
         
            return msg
        }
    func updateData()  {
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
        
        self.arrMonth =  ModelUserData.getUserDataFromUserDefaults()
            .deliveryPerMonth ?? ""
        
        self.arrPrice =  ModelUserData.getUserDataFromUserDefaults().subscriptionPrice ?? ""
        
        if ModelUserData.getUserDataFromUserDefaults().mealsPerDelivery == "" {
            self.lblValue.text = "0"
        }
        else {
            self.lblValue.text = ModelUserData.getUserDataFromUserDefaults().mealsPerDelivery
        }
        
        if ModelUserData.getUserDataFromUserDefaults().subscriptionStatus == "1" {
            
            self.btnUpdate.isUserInteractionEnabled = false
            self.btnUpdate.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.4666666667, blue: 0.4901960784, alpha: 0.5)
        }
        else {
            
            self.btnUpdate.isUserInteractionEnabled = true
            self.btnUpdate.backgroundColor = #colorLiteral(red: 0.9871097207, green: 0.4678505659, blue: 0.4897766113, alpha: 1)
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdate(_ sender: Any) {
        
        
        let msg = self.validation()
        if msg == "" {
            self.AddSubscription()
            
        }else{
            self.showToastMessage(message: msg)
        }
        
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        let value = Int(self.lblValue.text!)
        
        self.lblValue.text = "\(value! + 1)"
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        
        let value = Int(self.lblValue.text!)
        
        if value! > 1 {
            
            self.lblValue.text = "\(value! - 1)"
        }
    }
}

    
