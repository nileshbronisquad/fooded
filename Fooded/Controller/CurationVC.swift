//
//  CurationVC.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit

class CurationVC: UIViewController {
    
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var ContainerView: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> CurationVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "CurationVC") as! CurationVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnCusineChoise((UIButton).self)
        
        print("curatiion")
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCusineChoise(_ sender: Any) {
        view1.isHidden = false
        view2.isHidden = true
        
        self.view1.backgroundColor = Color_Hex(hex: "F85F6A")
        self.btn1.setTitleColor(Color_Hex(hex: "F85F6A"), for: .normal)
        self.btn2.setTitleColor(Color_Hex(hex: "000000"), for: .normal)

        ContainerView.subviews.forEach({ $0.removeFromSuperview() })
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: ContainerView.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor, constant: 0),
            ])
        
        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CusineChoiceVC") as! CusineChoiceVC
        addChild(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: ContainerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor)
            ])
        
        controller.didMove(toParent: self)
    }
    
    @IBAction func btnPrefferedRest(_ sender: Any) {
        view2.isHidden = false
        view1.isHidden = true
        
        self.view2.backgroundColor = Color_Hex(hex: "F85F6A")
        self.btn2.setTitleColor(Color_Hex(hex: "F85F6A"), for: .normal)
        self.btn1.setTitleColor(Color_Hex(hex: "000000"), for: .normal)
        
        ContainerView.subviews.forEach({ $0.removeFromSuperview() })
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: ContainerView.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor, constant: 0),
            ])
        
        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PrefferedRestaurentVC") as! PrefferedRestaurentVC
        addChild(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: ContainerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor)
            ])
        
        controller.didMove(toParent: self)
    }
    
    @IBAction func btnDietryRestrictions(_ sender: Any) {
        
        
        view1.isHidden = true
        view2.isHidden = true
       
        self.btn1.setTitleColor(Color_Hex(hex: "000000"), for: .normal)
        self.btn2.setTitleColor(Color_Hex(hex: "000000"), for: .normal)
        
        ContainerView.subviews.forEach({ $0.removeFromSuperview() })
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: ContainerView.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor, constant: 0),
            ])
        
        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DietryRestrictionVC") as! DietryRestrictionVC
        addChild(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        ContainerView.addSubview(controller.view)
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: ContainerView.leadingAnchor),
            controller.view.trailingAnchor.constraint(equalTo: ContainerView.trailingAnchor),
            controller.view.topAnchor.constraint(equalTo: ContainerView.topAnchor),
            controller.view.bottomAnchor.constraint(equalTo: ContainerView.bottomAnchor)
            ])
        
        controller.didMove(toParent: self)
    }
}
