//
//  UpdateProfileVC.swift
//  Fooded
//
//  Created by iMac on 13/02/21.
//

import UIKit
import Alamofire
import Toast_Swift
import SDWebImage

class UpdateProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var arrUpdateModel = [ModelUserData]()
    var selectedImage: UIImage?
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> UpdateProfileVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2
        imgProfile.clipsToBounds = true
        
        self.updateData()
        
        self.btnUpdate.applyShadowOnView()
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        let pick = UIImagePickerController()
        pick.delegate = self
        pick.sourceType = .photoLibrary
        pick.allowsEditing = true
        self.present(pick, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image1 =  info[UIImagePickerController.InfoKey.editedImage]as! UIImage
        self.imgProfile.image = image1
        selectedImage = image1
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnUpdate(_ sender: UIButton) {
        
        if selectedImage == nil {
            self.showToastMessage(message: kEmptyProfileImage)
        }
        else
        {
            let msg = self.validation()
            if msg == "" {
                
                self.UserProfileUpdate()
                // self.UploadImage(selectedImage!)
            }else{
                self.showToastMessage(message: msg)
            }
        }
    }
    
    @IBAction func btnReset(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtName.text == "" { // Mobile
            msg = kEmptyFirstName
        }
        else if
            txtEmail.text == ""{
            msg = kEmailempty
        }
        return msg
    }
    
     func updateData()  {
        
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
        
        print(userId)
        
        
        
        let strImg = "http://vbsd.co.in/fooded/upload/profile/" + ModelUserData.getUserDataFromUserDefaults().userProfile
        
        self.imgProfile.setImageFromURL(strImg,UIImage(named: "profile placeholder")!)
        
        
        let url = URL(string:strImg)
        if let data = try? Data(contentsOf: url!)
        {
            self.selectedImage = UIImage(data: data)
        }
        
        self.txtPhoneNumber.text =
            ModelUserData.getUserDataFromUserDefaults().mobile ?? ""
        self.txtName.text = ModelUserData.getUserDataFromUserDefaults().name ?? ""
        self.txtEmail.text =
            ModelUserData.getUserDataFromUserDefaults().email ?? ""
    }
    

    func UserProfileUpdate() {
        
        let imgData = selectedImage!.jpegData(compressionQuality: 0.5)
        
        let param: NSMutableDictionary = [
            "id" : ModelUserData.getUserDataFromUserDefaults().userId ?? 0,
            "name" : self.txtName.text!,
            "email" : self.txtEmail.text!,
            "user_profile" : imgData!
        ]
        showLoaderHUD(strMessage: "")
        
        HttpRequestManager.sharedInstance.requestWithPostMultipartParam(endpointurl: "app_update_profile", isImage: true, parameters: param, showLoader: true){(error, responseDic) in
            
            hideLoaderHUD()
            
            if let dic = responseDic {
                
                print(responseDic)
                
                if dic.value(forKey: kStatus) as? String ?? "" == "1"
                {
                   
                    let result = dic.value(forKey: "result") as! [String:Any]
                    let user_id = result["user_id"] as! String
                    let uId = Int(user_id)
                    print(uId)
                    
                    let model : ModelUserData = ModelUserData.init(fromDictionary: responseDic?.value(forKey: "result") as! [String : Any])
                    model.userId = uId
                    model.saveToUserDefaults()
                    
                    self.arrUpdateModel.removeAll()
                    self.arrUpdateModel.append(model)
                    self.updateData()

                    self.showToastMessage(message: dic.value(forKey: kMessage) as? String ?? "")
                    
                }
                else if dic.value(forKey: kStatus) as? String ?? "" == kStatusFail {
                    self.showToastMessage(message: dic.value(forKey: kMessage) as? String ?? "")
                }
            }
            
            else if let error = error {
                print(error)
                showMessage(error.localizedDescription)
            }
        }
    }
}
extension UIImageView {

     //Set Web Image
    func setImageFromURL(_ urlString: String,_ placeholder: UIImage = UIImage.init(named: "user_profile")!) {
        self.sd_setImage(with: URL(string: urlString), placeholderImage: placeholder)
    }

}
