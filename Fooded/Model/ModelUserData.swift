//
//	ModelUserData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelUserData : NSObject, NSCoding{
  
    var id : Int!
    var address : String!
    var city : String!
    var country : String!
    var deliveryPerMonth : String!
    var email : String!
    var mealsPerDelivery : String!
    var mobile : String!
    var name : String!
    var password : String!
    var postcode : String!
    var socialId : String!
    var socialType : String!
    var state : String!
    var subscriptionDate : String!
    var subscriptionPrice : String!
    var subscriptionStatus : String!
    var token : String!
    var userId : Int!
    var userProfile : String!
    var username : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["id"] as? Int

        address = dictionary["address"] as? String
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
        deliveryPerMonth = dictionary["delivery_per_month"] as? String
        email = dictionary["email"] as? String
        mealsPerDelivery = dictionary["meals_per_delivery"] as? String
        mobile = dictionary["mobile"] as? String
        name = dictionary["name"] as? String
        password = dictionary["password"] as? String
        postcode = dictionary["postcode"] as? String
        socialId = dictionary["social_id"] as? String
        socialType = dictionary["social_type"] as? String
        state = dictionary["state"] as? String
        subscriptionDate = dictionary["subscription_date"] as? String
        subscriptionPrice = dictionary["subscription_price"] as? String
        subscriptionStatus = dictionary["subscription_status"] as? String
        token = dictionary["token"] as? String
        userId = dictionary["user_id"] as? Int
        userProfile = dictionary["user_profile"] as? String
        username = dictionary["username"] as? String
    }
    
    override init() {
        userId = Int()
        name = ""
        email = ""
        username = ""
        mobile = ""
        userProfile = ""
        address = ""
        city = ""
        state = ""
        country = ""
        postcode = ""
        socialType = ""
        socialId = ""
        token = ""
        deliveryPerMonth = ""
        mealsPerDelivery = ""
        subscriptionPrice = ""
        subscriptionStatus = ""
        subscriptionDate = ""
        id = Int()

    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if address != nil{
            dictionary["address"] = address
        }
        if city != nil{
            dictionary["city"] = city
        }
        if country != nil{
            dictionary["country"] = country
        }
        if deliveryPerMonth != nil{
            dictionary["delivery_per_month"] = deliveryPerMonth
        }
        if email != nil{
            dictionary["email"] = email
        }
        if mealsPerDelivery != nil{
            dictionary["meals_per_delivery"] = mealsPerDelivery
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if name != nil{
            dictionary["name"] = name
        }
        if password != nil{
            dictionary["password"] = password
        }
        if postcode != nil{
            dictionary["postcode"] = postcode
        }
        if socialId != nil{
            dictionary["social_id"] = socialId
        }
        if socialType != nil{
            dictionary["social_type"] = socialType
        }
        if state != nil{
            dictionary["state"] = state
        }
        if subscriptionDate != nil{
            dictionary["subscription_date"] = subscriptionDate
        }
        if subscriptionPrice != nil{
            dictionary["subscription_price"] = subscriptionPrice
        }
        if subscriptionStatus != nil{
            dictionary["subscription_status"] = subscriptionStatus
        }
        if token != nil{
            dictionary["token"] = token
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if userProfile != nil{
            dictionary["user_profile"] = userProfile
        }
        if username != nil{
            dictionary["username"] = username
        }
        return dictionary
    }


    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int

         address = aDecoder.decodeObject(forKey: "address") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         deliveryPerMonth = aDecoder.decodeObject(forKey: "delivery_per_month") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         mealsPerDelivery = aDecoder.decodeObject(forKey: "meals_per_delivery") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         postcode = aDecoder.decodeObject(forKey: "postcode") as? String
         socialId = aDecoder.decodeObject(forKey: "social_id") as? String
         socialType = aDecoder.decodeObject(forKey: "social_type") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         subscriptionDate = aDecoder.decodeObject(forKey: "subscription_date") as? String
         subscriptionPrice = aDecoder.decodeObject(forKey: "subscription_price") as? String
         subscriptionStatus = aDecoder.decodeObject(forKey: "subscription_status") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int
         userProfile = aDecoder.decodeObject(forKey: "user_profile") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if deliveryPerMonth != nil{
            aCoder.encode(deliveryPerMonth, forKey: "delivery_per_month")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if mealsPerDelivery != nil{
            aCoder.encode(mealsPerDelivery, forKey: "meals_per_delivery")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if password != nil{
            aCoder.encode(password, forKey: "password")
        }
        if postcode != nil{
            aCoder.encode(postcode, forKey: "postcode")
        }
        if socialId != nil{
            aCoder.encode(socialId, forKey: "social_id")
        }
        if socialType != nil{
            aCoder.encode(socialType, forKey: "social_type")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if subscriptionDate != nil{
            aCoder.encode(subscriptionDate, forKey: "subscription_date")
        }
        if subscriptionPrice != nil{
            aCoder.encode(subscriptionPrice, forKey: "subscription_price")
        }
        if subscriptionStatus != nil{
            aCoder.encode(subscriptionStatus, forKey: "subscription_status")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if userProfile != nil{
            aCoder.encode(userProfile, forKey: "user_profile")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }

    }
    
    func saveToUserDefaults() {
           let data = NSKeyedArchiver.archivedData(withRootObject: self)
           USERDEFAULTS.set(data, forKey: kModelUserData)
       }
       class func getUserDataFromUserDefaults() -> ModelUserData {
              if let data = USERDEFAULTS.value(forKey: kModelUserData) as? Data {
                  return NSKeyedUnarchiver.unarchiveObject(with: data) as? ModelUserData ?? ModelUserData()
              }
              return ModelUserData()
          }

}
