//
//	UserListModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserListModel : NSObject, NSCoding{

	var id : Int!
	var isAllergenFriendly : String!
	var isGivtenFree : String!
	var isKetogenic : String!
	var isLactoseIntrollerant : String!
	var isPaleogenic : String!
	var isVegan : String!
	var mealPerDeliveryCount : String!
	var subUserName : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["id"] as? Int
		isAllergenFriendly = dictionary["is_allergen_friendly"] as? String
		isGivtenFree = dictionary["is_givten_free"] as? String
		isKetogenic = dictionary["is_ketogenic"] as? String
		isLactoseIntrollerant = dictionary["is_lactose_introllerant"] as? String
		isPaleogenic = dictionary["is_paleogenic"] as? String
		isVegan = dictionary["is_vegan"] as? String
		mealPerDeliveryCount = dictionary["meal_per_delivery_count"] as? String
		subUserName = dictionary["sub_user_name"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if isAllergenFriendly != nil{
			dictionary["is_allergen_friendly"] = isAllergenFriendly
		}
		if isGivtenFree != nil{
			dictionary["is_givten_free"] = isGivtenFree
		}
		if isKetogenic != nil{
			dictionary["is_ketogenic"] = isKetogenic
		}
		if isLactoseIntrollerant != nil{
			dictionary["is_lactose_introllerant"] = isLactoseIntrollerant
		}
		if isPaleogenic != nil{
			dictionary["is_paleogenic"] = isPaleogenic
		}
		if isVegan != nil{
			dictionary["is_vegan"] = isVegan
		}
		if mealPerDeliveryCount != nil{
			dictionary["meal_per_delivery_count"] = mealPerDeliveryCount
		}
		if subUserName != nil{
			dictionary["sub_user_name"] = subUserName
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isAllergenFriendly = aDecoder.decodeObject(forKey: "is_allergen_friendly") as? String
         isGivtenFree = aDecoder.decodeObject(forKey: "is_givten_free") as? String
         isKetogenic = aDecoder.decodeObject(forKey: "is_ketogenic") as? String
         isLactoseIntrollerant = aDecoder.decodeObject(forKey: "is_lactose_introllerant") as? String
         isPaleogenic = aDecoder.decodeObject(forKey: "is_paleogenic") as? String
         isVegan = aDecoder.decodeObject(forKey: "is_vegan") as? String
         mealPerDeliveryCount = aDecoder.decodeObject(forKey: "meal_per_delivery_count") as? String
         subUserName = aDecoder.decodeObject(forKey: "sub_user_name") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isAllergenFriendly != nil{
			aCoder.encode(isAllergenFriendly, forKey: "is_allergen_friendly")
		}
		if isGivtenFree != nil{
			aCoder.encode(isGivtenFree, forKey: "is_givten_free")
		}
		if isKetogenic != nil{
			aCoder.encode(isKetogenic, forKey: "is_ketogenic")
		}
		if isLactoseIntrollerant != nil{
			aCoder.encode(isLactoseIntrollerant, forKey: "is_lactose_introllerant")
		}
		if isPaleogenic != nil{
			aCoder.encode(isPaleogenic, forKey: "is_paleogenic")
		}
		if isVegan != nil{
			aCoder.encode(isVegan, forKey: "is_vegan")
		}
		if mealPerDeliveryCount != nil{
			aCoder.encode(mealPerDeliveryCount, forKey: "meal_per_delivery_count")
		}
		if subUserName != nil{
			aCoder.encode(subUserName, forKey: "sub_user_name")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}