//
//	Restaurant.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Restaurant : NSObject, NSCoding{

	var id : Int!
	var isSelect : String!
	var restaurantAddress : String!
	var restaurantCity : String!
	var restaurantCountry : String!
	var restaurantImage : String!
	var restaurantName : String!
	var restaurantPostcode : String!
	var restaurantRatingCount : String!
	var restaurantRatingStatus : Int!
	var restaurantState : String!
	var restaurantType : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["id"] as? Int
		isSelect = dictionary["is_select"] as? String
		restaurantAddress = dictionary["restaurant_address"] as? String
		restaurantCity = dictionary["restaurant_city"] as? String
		restaurantCountry = dictionary["restaurant_country"] as? String
		restaurantImage = dictionary["restaurant_image"] as? String
		restaurantName = dictionary["restaurant_name"] as? String
		restaurantPostcode = dictionary["restaurant_postcode"] as? String
		restaurantRatingCount = dictionary["restaurant_rating_count"] as? String
		restaurantRatingStatus = dictionary["restaurant_rating_status"] as? Int
		restaurantState = dictionary["restaurant_state"] as? String
		restaurantType = dictionary["restaurant_type"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if isSelect != nil{
			dictionary["is_select"] = isSelect
		}
		if restaurantAddress != nil{
			dictionary["restaurant_address"] = restaurantAddress
		}
		if restaurantCity != nil{
			dictionary["restaurant_city"] = restaurantCity
		}
		if restaurantCountry != nil{
			dictionary["restaurant_country"] = restaurantCountry
		}
		if restaurantImage != nil{
			dictionary["restaurant_image"] = restaurantImage
		}
		if restaurantName != nil{
			dictionary["restaurant_name"] = restaurantName
		}
		if restaurantPostcode != nil{
			dictionary["restaurant_postcode"] = restaurantPostcode
		}
		if restaurantRatingCount != nil{
			dictionary["restaurant_rating_count"] = restaurantRatingCount
		}
		if restaurantRatingStatus != nil{
			dictionary["restaurant_rating_status"] = restaurantRatingStatus
		}
		if restaurantState != nil{
			dictionary["restaurant_state"] = restaurantState
		}
		if restaurantType != nil{
			dictionary["restaurant_type"] = restaurantType
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isSelect = aDecoder.decodeObject(forKey: "is_select") as? String
         restaurantAddress = aDecoder.decodeObject(forKey: "restaurant_address") as? String
         restaurantCity = aDecoder.decodeObject(forKey: "restaurant_city") as? String
         restaurantCountry = aDecoder.decodeObject(forKey: "restaurant_country") as? String
         restaurantImage = aDecoder.decodeObject(forKey: "restaurant_image") as? String
         restaurantName = aDecoder.decodeObject(forKey: "restaurant_name") as? String
         restaurantPostcode = aDecoder.decodeObject(forKey: "restaurant_postcode") as? String
         restaurantRatingCount = aDecoder.decodeObject(forKey: "restaurant_rating_count") as? String
         restaurantRatingStatus = aDecoder.decodeObject(forKey: "restaurant_rating_status") as? Int
         restaurantState = aDecoder.decodeObject(forKey: "restaurant_state") as? String
         restaurantType = aDecoder.decodeObject(forKey: "restaurant_type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isSelect != nil{
			aCoder.encode(isSelect, forKey: "is_select")
		}
		if restaurantAddress != nil{
			aCoder.encode(restaurantAddress, forKey: "restaurant_address")
		}
		if restaurantCity != nil{
			aCoder.encode(restaurantCity, forKey: "restaurant_city")
		}
		if restaurantCountry != nil{
			aCoder.encode(restaurantCountry, forKey: "restaurant_country")
		}
		if restaurantImage != nil{
			aCoder.encode(restaurantImage, forKey: "restaurant_image")
		}
		if restaurantName != nil{
			aCoder.encode(restaurantName, forKey: "restaurant_name")
		}
		if restaurantPostcode != nil{
			aCoder.encode(restaurantPostcode, forKey: "restaurant_postcode")
		}
		if restaurantRatingCount != nil{
			aCoder.encode(restaurantRatingCount, forKey: "restaurant_rating_count")
		}
		if restaurantRatingStatus != nil{
			aCoder.encode(restaurantRatingStatus, forKey: "restaurant_rating_status")
		}
		if restaurantState != nil{
			aCoder.encode(restaurantState, forKey: "restaurant_state")
		}
		if restaurantType != nil{
			aCoder.encode(restaurantType, forKey: "restaurant_type")
		}

	}

}