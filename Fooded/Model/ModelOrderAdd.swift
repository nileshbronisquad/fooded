//
//	ModelOrderAdd.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelOrderAdd : NSObject, NSCoding{

	var amount : String!
	var deliveryDate : String!
	var id : Int!
	var lessSubscriptionOrder : String!
	var orderStatus : String!
	var preferencesId : String!
	var restaurantId : String!
	var transactionId : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		amount = dictionary["amount"] as? String
		deliveryDate = dictionary["delivery_date"] as? String
		id = dictionary["id"] as? Int
		lessSubscriptionOrder = dictionary["less_subscription_order"] as? String
		orderStatus = dictionary["order_status"] as? String
		preferencesId = dictionary["preferences_id"] as? String
		restaurantId = dictionary["restaurant_id"] as? String
		transactionId = dictionary["transaction_id"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if amount != nil{
			dictionary["amount"] = amount
		}
		if deliveryDate != nil{
			dictionary["delivery_date"] = deliveryDate
		}
		if id != nil{
			dictionary["id"] = id
		}
		if lessSubscriptionOrder != nil{
			dictionary["less_subscription_order"] = lessSubscriptionOrder
		}
		if orderStatus != nil{
			dictionary["order_status"] = orderStatus
		}
		if preferencesId != nil{
			dictionary["preferences_id"] = preferencesId
		}
		if restaurantId != nil{
			dictionary["restaurant_id"] = restaurantId
		}
		if transactionId != nil{
			dictionary["transaction_id"] = transactionId
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         deliveryDate = aDecoder.decodeObject(forKey: "delivery_date") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         lessSubscriptionOrder = aDecoder.decodeObject(forKey: "less_subscription_order") as? String
         orderStatus = aDecoder.decodeObject(forKey: "order_status") as? String
         preferencesId = aDecoder.decodeObject(forKey: "preferences_id") as? String
         restaurantId = aDecoder.decodeObject(forKey: "restaurant_id") as? String
         transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if deliveryDate != nil{
			aCoder.encode(deliveryDate, forKey: "delivery_date")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lessSubscriptionOrder != nil{
			aCoder.encode(lessSubscriptionOrder, forKey: "less_subscription_order")
		}
		if orderStatus != nil{
			aCoder.encode(orderStatus, forKey: "order_status")
		}
		if preferencesId != nil{
			aCoder.encode(preferencesId, forKey: "preferences_id")
		}
		if restaurantId != nil{
			aCoder.encode(restaurantId, forKey: "restaurant_id")
		}
		if transactionId != nil{
			aCoder.encode(transactionId, forKey: "transaction_id")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}