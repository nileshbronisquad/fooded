//
//  CusineChoiceCell.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit

class CusineChoiceCell: UITableViewCell {

    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var lblFoodname: UILabel!
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var bgStepper: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgStepper.myViewCorners()
        self.imgFood.myViewCorners()
        self.bgStepper.layer.borderColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 0.4)
        self.bgStepper.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnPlus(_ sender: Any) {
     
            
            let value = Int(self.lblValue.text!)
            
            self.lblValue.text = "\(value! + 1)"
        
        
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        let value = Int(self.lblValue.text!)
        
        if value! > 1 {
            
            self.lblValue.text = "\(value! - 1)"
            
        }
        
        if value == 1 {
            
            self.lblValue.text = "\(value! - 1)"
        }
    
    }

    
    
}
