//
//  UserCollectionViewCell.swift
//  Fooded
//
//  Created by macbook on 26/04/21.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgViewUser: UIView!
    @IBOutlet weak var lblUser: UILabel!
    override func awakeFromNib() {
        self.bgViewUser.myViewCorners()
        self.lblUser.myViewCorners()
        super.awakeFromNib()
        // Initialization code
    }

}
