//
//  PrefferedRestCell.swift
//  Fooded
//
//  Created by iMac on 12/02/21.
//

import UIKit

class PrefferedRestCell: UITableViewCell {
    var isbtn = false
    @IBOutlet weak var ratingView: FloatRatingView!
    var arrRestaurant = [Restaurant]()
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgRestaurant: UIImageView!
    @IBOutlet weak var lblRestaurantType: UILabel!
    @IBOutlet weak var lblRestaurantAddress: UILabel!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var bgRating: UIView!
    @IBOutlet weak var bgCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.bgRating.layer.cornerRadius = 2
        self.bgCell.myViewCorners()
    }
    
    @IBAction func btnCheckbox(_ sender: Any) {

}

}
