//
//  AppDelegate.swift
//  BookFast
//
//  Created by Nilesh Desai on 05/01/21.
//

import UIKit
import MFSideMenu


@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let userId = ModelUserData.getUserDataFromUserDefaults().userId ?? 0
        if userId == 0{
        }
        else{
            goToWelcome()
        }
        
        
        return true
    }
    
   
    func applicationWillResignActive(_ application: UIApplication) {
      
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
     
    }
    
    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    func goToWelcome()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let rootNavigationController:UINavigationController = appdelegate.window?.rootViewController as! UINavigationController
        let navigationController:UINavigationController = storyBoard.instantiateViewController(withIdentifier: "navHome")  as! UINavigationController
        let sideMenuVc : SideMenuVC = storyBoard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let menuContainer:MFSideMenuContainerViewController = MFSideMenuContainerViewController.container(withCenter: navigationController, leftMenuViewController: sideMenuVc, rightMenuViewController: nil)
        menuContainer.shadow.enabled = true
        menuContainer.panMode = MFSideMenuPanMode(rawValue: 1)
        rootNavigationController.pushViewController(menuContainer, animated: false)
    }

}

