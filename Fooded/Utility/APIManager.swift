//
//  APIManager.swift
//
//  Created by Developer on 21/03/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import MobileCoreServices
import Alamofire
import SVProgressHUD

class APIManager: NSObject {
    
    typealias APIManagerResponseHandler = (NSDictionary)->()
    typealias APIManagerErrorHandler = (Int,String)->()
    
    // Check internet connection
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    
    //MARK: GET
    class func getRequest(service:String,
                    showLoader:Bool,
                    urlParam:NSMutableDictionary,
                    success:@escaping APIManagerResponseHandler, failure: APIManagerErrorHandler? = nil)
    {
        if isConnectedToNetwork()
        {
            if showLoader { showLoader }
            
            let headers : HTTPHeaders = [ "Content-Type": "application/json",
                            "pnp_access_key": "sift_app_access_api_pnp" ]
            
            let lat = "\(urlParam.value(forKey: "latitude") as! String)"
            let log = "\(urlParam.value(forKey: "longitude") as! String)"
            
            let strURL = "\(service)latlng=\(lat),\(log)&key=\(Constant.kGoogleAPIKey)"
            Alamofire.Session.default.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                
                switch response.result{
                case .success(let value):
                  
                    if let jsonDict = value as? NSDictionary {
                        print("Response :- \(jsonDict)\n\n\n")
                        
                        success(jsonDict)
                    }
                    
                    //success(jsonDict)responseData(nil, dictionaryOfFilteredBy(dict: (value as? NSDictionary)!))
                    break
                case .failure(let error):
                    if showLoader {
                        Utility.showAlert(Constant.ErrorMessage.kTitle, message: Constant.ErrorMessage.kCommanError)
                    }
                    failure?(1, Constant.ErrorMessage.kCommanError)
                   // responseData(error as NSError?, nil)
                    break
                }
            }
            
        }
    }
     
}
