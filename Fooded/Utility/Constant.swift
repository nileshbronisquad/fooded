//
//  Constant.swift
//
//  Created by Developer on 13/03/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

@available(iOS 13.0, *)
let kAppDelegate                =   UIApplication.shared.delegate as! AppDelegate
@available(iOS 13.0, *)
let kRootViewController         =   kAppDelegate.window?.rootViewController
let kMainStoryBoard             =   UIStoryboard(name: "Main", bundle: nil)
let USERDEFAULTS                =   UserDefaults.standard
let kImgPlaceHolder             =   UIImage(named: "photos_blankIcon")!

let kSelectTitle        =   "Please enter title."
let kEnterDescription   =   "Please enter description."
let kEnterPriority      =   "Please select priority."

var currentLocation: CLLocation!             = CLLocation(latitude: 5.593222, longitude: -0.140138)//CLLocation(latitude: 21.2423, longitude: 72.8781)
var mycurrentLocation: CLLocation!           = CLLocation(latitude: 5.593222, longitude: -0.140138)

class Constant: NSObject {
    static var env = "prod"
    static var location: CLLocation?
    static var ApiLattitude = ""
    static var Apilongitude = ""
    static var ApiAddress = ""
    static let kAppname                     =   "QSS"
    static let kDeviceType                  =   "1" // device type 1 = iOS, 2 = Android
    static let kLangType                    =   "1"//"LANG_TYPE".localized
    static var kDeviceToken                 =   "123"
    static let kFBURLScheme                 =   "fb428849964630198"
    static let kGoogleAPIKey                =   "AIzaSyBfAIr-zOOS_3qBMKB19nIrgza7Mm3kLPI"
    
    // MARK:- API Constant
    struct ServerAPI {
        
        static let kBaseURL                         = "http://vbsd.co.in/fooded/api/"
        
        

    }
    
    // MARK:- Pop Up Error Message
    struct ErrorMessage {
        static let kTitle                       =   ""
        static let kNoInternetConnection        =   "NO INTERNET CONNECT"
        static let kCommanError                 =   "UNEXPECTED ERROR"
        static let kServerError                 =   "SERVER ERROR"
        static let kRequestForbidden            =   "Request failed: forbidden (403)"
        static let kRequestUnauthorize          =   "Request failed: unauthorized (401)"
    }
    
    class var USERS_ENVIROMENT: String {
        #if DEBUG
        return "dev"
        #elseif QA
        return "qbqa"
        #else
        assert(false, "Not supported build configuration")
        return ""
        #endif
    }
}




// MARK:- Structure for User Defaults Key
struct UDKey {
    
    static let kUserInfo                    =   "UserInfo"
    static let kGroupInfo                   =   "GroupInfo"
    static let kuserBankInfo                =   "userBankInfo"
    static let kIsProfileSetup              =   "IsProfileSetup"
    static let kLastOpenedScreen            =   "LastOpenedScreen"
    static let kIsLoginWithGust             =   "IsLoginWithGust"
    
}

// MARK:- Device Idiom
enum UIUserInterfaceIdiom : Int {
    case Unspecified
    case Phone
    case Pad
}

// MARK:- Screen Size
struct ScreenSize {
    static let SCREEN_WIDTH             =   UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT            =   UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH        =   max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH        =   min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

// MARK:- Device Type
struct DeviceType {
    static let IS_IPHONE_4_OR_LESS      =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_8              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_8P             =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X              =   UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD                  =   UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO              =   UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}


enum LastScreen: String, Decodable {
    
    case defaultScreen      =   "0"
    case Dashboard          =   "1"
    case Profile            =   "2"
    case Booking            =   "3"
    case OfferAds           =   "5"
    case Setting            =   "6"
}

