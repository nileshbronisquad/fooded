//
//	PreferencesModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PreferencesModel : NSObject, NSCoding{

	var id : Int!
	var preferencesImage : String!
	var preferencesName : String!
	var preferencesPrice : String!

    override init() {
        id = Int()
        preferencesImage = ""
        preferencesName = ""
        preferencesPrice = ""
    
    
    }
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["id"] as? Int
		preferencesImage = dictionary["preferences_image"] as? String
		preferencesName = dictionary["preferences_name"] as? String
		preferencesPrice = dictionary["preferences_price"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if preferencesImage != nil{
			dictionary["preferences_image"] = preferencesImage
		}
		if preferencesName != nil{
			dictionary["preferences_name"] = preferencesName
		}
		if preferencesPrice != nil{
			dictionary["preferences_price"] = preferencesPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         preferencesImage = aDecoder.decodeObject(forKey: "preferences_image") as? String
         preferencesName = aDecoder.decodeObject(forKey: "preferences_name") as? String
         preferencesPrice = aDecoder.decodeObject(forKey: "preferences_price") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if preferencesImage != nil{
			aCoder.encode(preferencesImage, forKey: "preferences_image")
		}
		if preferencesName != nil{
			aCoder.encode(preferencesName, forKey: "preferences_name")
		}
		if preferencesPrice != nil{
			aCoder.encode(preferencesPrice, forKey: "preferences_price")
		}

	}

}
